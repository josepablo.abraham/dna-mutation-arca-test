# ARCA DNA Mutation Test Application

## Purpose

The purpose of this application is to generate a web application with a
REST API interface which enables the user to upload DNA samples in the following
format:

```json
{
  "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

Where the number of array items must be equal to the number of letters inside an array
element (an NxN matrix). And only the following letters are allowed `['T', 'A', 'C', 'G']`.

The application has two endpoints that can be used:

### **/mutation** _POST_

This endpoint is designed to upload a sample as described in the first section by POSTing
the data in the following format:

```json
{
  "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

If the structure is valid, it will then be processed and the endpoint will return the following structure:

```json
{
  "hasMutation": false
}
```

Where `hasMutation` will either be `true` or `false` depending on the DNA structure sent.

### **/stats** _GET_

This endpoint returns general stats of all the VALID samples uploaded to the endpoint,
the response follows the following structure:

```json
{
  "count_mutations": 5,
  "count_no_mutation": 4,
  "ratio": 1.25
}
```


## How to run locally:

### Pre-requirements:

- [`Node.js`] (v12 or newer)
- [`npm`] (v6 or newer)
- [`AWS Account`]
- [`AWS JavaScript SDK`] (v2.792 or newer)
- [`DynamoDB`] local instance (optional)


In order to use the AWS Javascript SDK programatically, you first need to generate a User access key and secret key within the AWS Account Management console. See more details [here](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SettingUp.DynamoWebService.html).

### Installation

After making sure that all the requirements are met clone or download this repository and
within the created directory run:

```cmd
npm install
```

This will install all required dependencies for running the application on your computer.

You can also optionally modify the file parameters inside `app/config.js` before running the application.

While ensuring that the keys for using the AWS SDK are set properly, run the following:

```cmd
node app/local/createTable.js
```

This will setup the required table to use within the application.

If you would like to use a local dynamoDB instance within the application, you can do so by uncommenting the line 'endpoint' within `app/lib/dynamo/dynamodb.js`.

### Running the application

Do this section only after doing the previous sections and making sure everything is configured correctly.

To start the application simply run the following command:

```cmd
npm start
```


[`Node.js`]: https://nodejs.org/
[`npm`]: https://www.npmjs.com/
[`AWS Account`]: https://signin.aws.amazon.com/signin
[`AWS JavaScript SDK`]: https://aws.amazon.com/sdk-for-node-js/
[`DynamoDB`]: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html
