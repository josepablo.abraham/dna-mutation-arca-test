var config = {};

config.dynamo = {};
config.express = {};

config.dynamo.region = process.env.AWS_REGION || "us-east-1";
config.dynamo.hostname = "http://localhost";
config.dynamo.port = "8000";
config.dynamo.tableName =  process.env.DNA_TABLE || "DNAResults";
config.express.port = process.env.PORT || 8081;

module.exports = config;
