const express = require("express");
const config = require("./config");
const app = express();
const port = config.express.port;

const analyze = require("./analyze/analyze");
const stats = require("./stats/stats");

app.use(express.json());

app.post("/mutation", (req, res) => {
  analyze(req, res);
});

app.get("/stats", (_, res, next) => {
  stats(res, next);
});

app.listen(port, () => {
  console.log(`Application ready on port: ${port}`);
});
