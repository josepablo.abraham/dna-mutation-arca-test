const dynamodb = require("../lib/dynamo/dynamodb");

const config = require("../config");
const tableName = config.dynamo.tableName;

function stats(res, next) {
  const params = {
    ExpressionAttributeValues: {
      ":m": {
        BOOL: true,
      },
    },
    FilterExpression: "hasMutation = :m",
    TableName: tableName,
    Select: "COUNT",
  };

  dynamodb.scan(params, function (err, data) {
    if (err) {
      console.error(err, err.stack);
      res.sendStatus(500);
      res.send({
        error: "Stats operation error. DB Communication error!",
      });
    } else {
      const count_mutations = data.Count || 0;
      const count_no_mutation = data.ScannedCount - data.Count || 0;
      const ratio =
        count_no_mutation !== 0 ? count_mutations / count_no_mutation : 0;
      res.send({
        count_mutations,
        count_no_mutation,
        ratio,
      });
    }
    next();
  });
}

module.exports = stats;
