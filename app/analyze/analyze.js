const { v4: uuidv4 } = require("uuid");

const mutation = require("../lib/hasMutation");

const dynamodb = require("../lib/dynamo/dynamodb");

const config = require("../config");
const tableName = config.dynamo.tableName;

function saveResult(value) {
  const params = {
    TableName: tableName,
    Item: {
      id: {
        S: uuidv4(),
      },
      hasMutation: {
        BOOL: value,
      },
    },
  };

  dynamodb.putItem(params, function (err) {
    if (err) {
      console.error(
        "Unable to add item. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    }
  });
}

function analyze(req, res) {
  const data = req.body || undefined;
  if (mutation.validateArguments(data.dna)) {
    try {
      const result = mutation.hasMutation(data.dna);
      saveResult(result);
      res.send({
        hasMutation: result,
      });
    } catch (e) {
      res.sendStatus(500);
      res.send({
        error: "An error has ocurred trying to analyze DNA data!",
      });
    }
  } else {
    res.sendStatus(403);
    res.send({
      error: "Parameters are invalid!",
    });
  }
}

module.exports = analyze;
