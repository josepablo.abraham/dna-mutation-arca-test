const CHAR_LIMIT = 4;

function hasMutation(arg) {
  // if (!validateArguments(arg)) {
  //   throw new Error("Invalid input arguments found");
  // }

  return !(
    validateH(arg) &&
    validateV(arg) &&
    validateO_LR(arg) &&
    validateO_RL(arg)
  );
}

function validateArguments(arg) {
  if (!Array.isArray(arg) && arg.length === 0) {
    return false;
  }

  if (typeof arg[0] !== "string" || arg[0].length === 0) {
    return false;
  }

  const len = arg[0].length;
  let invalid = false;

  for (const a of arg) {
    if (
      typeof a !== "string" ||
      a.length !== len ||
      hasInvalidChars(a.toLowerCase(), ["a", "t", "g", "c"])
    ) {
      invalid = true;
      break;
    }
  }

  return !invalid && arg.length === len;
}

function hasInvalidChars(input, chars) {
  const chrArr = Array.from(input);

  for (const ch of chrArr) {
    if (!chars.includes(ch)) {
      return true;
    }
  }

  return false;
}

function validateH(arg) {
  for (const str of arg) {
    const chrArr = Array.from(str);

    let count = 0;
    let prevChar = undefined;

    for (const letter of chrArr) {
      if (prevChar === letter) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = letter;
    }
  }
  return true;
}

function validateV(arg) {
  for (let i = 0; i < arg[0].length; i++) {
    let count = 0;
    let prevChar = undefined;
    for (let j = 0; j < arg[0].length; j++) {
      if (prevChar === arg[j][i]) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = arg[j][i];
    }
  }
  return true;
}

function validateO_LR(arg) {
  const len = arg[0].length;
  for (let i = 0; i <= len - CHAR_LIMIT; i++) {
    let count = 0;
    let prevChar = undefined;
    for (let j = 0; j < len; j++) {
      if (i + j >= len) {
        break;
      }
      if (prevChar === arg[j][i + j]) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = arg[j][i + j];
    }
  }
  for (let i = 1; i <= len - CHAR_LIMIT; i++) {
    let count = 0;
    let prevChar = undefined;
    for (let j = 0; j < len; j++) {
      if (i + j >= len) {
        break;
      }
      if (prevChar === arg[i + j][j]) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = arg[i + j][j];
    }
  }
  return true;
}

function validateO_RL(arg) {
  const len = arg[0].length;
  for (let i = len - 1; i > len - CHAR_LIMIT; i--) {
    let count = 0;
    let prevChar = undefined;
    for (let j = 0; j < len; j++) {
      if (i - j < 0) {
        break;
      }
      if (prevChar === arg[j][i - j]) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = arg[j][i - j];
    }
  }
  for (let i = len - 2; i > len - CHAR_LIMIT; i--) {
    let count = 0;
    let prevChar = undefined;
    for (let j = 0; j < len; j++) {
      if (i - j < 0) {
        break;
      }
      if (prevChar === arg[i - j][j]) {
        count++;

        if (count >= CHAR_LIMIT) {
          return false;
        }
      } else {
        count = 1;
      }
      prevChar = arg[i - j][j];
    }
  }
  return true;
}

function test() {
  console.log(
    '"ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"',
    "No Mutation: ",
    hasMutation(["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"])
  );

  console.log(
    '"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"',
    "Has Mutation: ",
    hasMutation(["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"])
  );
}

/**
 * Exported functions for used within a dependency.
 */
exports.hasMutation = hasMutation;
exports.validateArguments = validateArguments;
exports.test = test;
