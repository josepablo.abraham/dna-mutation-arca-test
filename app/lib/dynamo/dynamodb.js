const config = require("../../config");
const AWS = require("aws-sdk");

// Uncomment 'endpoint' if using a local dynamoDB instance.
AWS.config.update({
  region: config.dynamo.region,
  // endpoint: `${config.dynamo.hostname}:${config.dynamo.port}`,
});

const db = new AWS.DynamoDB();

module.exports = db;
