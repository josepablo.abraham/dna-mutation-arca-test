const config = require("../config");
const dynamodb = require("../lib/dynamo/dynamodb");

const tableName = config.dynamo.tableName;

function createDNATable() {
  const params = {
    TableName: tableName,
    KeySchema: [
      { AttributeName: "id", KeyType: "HASH" },
    ],
    AttributeDefinitions: [
      { AttributeName: "id", AttributeType: "S" },
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10,
    },
  };

  dynamodb.createTable(params, function (err, data) {
    if (err) {
      console.error(
        "Unable to create table. Error JSON:",
        JSON.stringify(err, null, 2)
      );
    } else {
      console.log(
        "Created table. Table description JSON:",
        JSON.stringify(data, null, 2)
      );
    }
  });
}

dynamodb.listTables({}, (err, data) => {
  if (err) {
    console.log(err, err.stack);
  } else {
    if (Array.isArray(data.TableNames)) {
      if (!data.TableNames.includes(tableName)) {
        createDNATable();
      } else {
        console.log("Table already exists!");
      }
    } else {
      console.error("listTable cmd is not formatted properly!!");
    }
  }
});
